# -*- coding: utf-8 -*-

# Scrapy settings for investor_relations_crawler project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'investor_relations_crawler'

SPIDER_MODULES = ['investor_relations_crawler.spiders']
NEWSPIDER_MODULE = 'investor_relations_crawler.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'investor_relations_crawler (+http://www.yourdomain.com)'
