#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle
import sys
import math
import os

import iso_Graph


verbose = False

def string_to_int(s):
    if s is None:
        return None
    else:
        try:
            return int(float(s))
        except ValueError:
            return None



def parse(filenames, add_0=True):
    """ calculate names and locations.
        Names are emissions: |US->['US','USA', etc], represented as dict
        Locations are hierarchies: '|0|GB|England|London|Richmond': 200000, represented as dict"""
    names = {}
    locations = []
    for filename in filenames:
        with open(filename, 'r') as f:
            for line in f:
                if line == '':
                    continue
                if add_0:
                    line = '|0'+line.strip()
                else:
                    line = line.strip()
                spl = line.split('->')
                if len(spl) == 2:
                    [x,y] = spl
                    # it's an emission X->Y
                    if not names.has_key(x):
                        names[x] = []
                    names[x].append(y)
                else:
                    locations.append(line)
    return names, locations
        
if len(sys.argv) < 3:
    sys.stderr.write("Please provide filename of pgm definition file (json fmt)\n")
    sys.stderr.write("make_graph.py <filename1> [<filename2> <filename3>...] <outfile.pkl>\n")
    sys.exit(1)
ifn = sys.argv[1:len(sys.argv)-1]
ofn = sys.argv[len(sys.argv)-1]

ofn_ext = os.path.splitext(ofn)[1]
if ofn_ext != '.pkl':
    sys.stderr.write("'%s' should have the extension .pkl instead of %s\n" % (ofn, ofn_ext))
    sys.exit(1)

sys.stderr.write("Input:\n")
for i, fn in enumerate(ifn):
    sys.stderr.write(" %d. '%s'\n" % (i+1, fn))
sys.stderr.write("Output:\n    '%s'\n" % ofn)

names, locations = parse(ifn)
#print 'names=',names
#print 'locations=',locations

# work out the counts for the different locations in the hierarchy
sys.stderr.write('Calulate counts')
counts = {}
countries = {}
count = 0   # progress counter
for loc in locations:
    if loc == '|0':
        continue
    try:
        loc, pop = loc.split(':')
    except:
        sys.stderr.write("Problem with location '%s', cannot split on ':'\n" % loc)
        sys.exit(1)
    pop = string_to_int(pop)
    l = loc.split('|')
    countries['|'+l[1]] = 1
    for name in  ['|'.join(l[0:e]) for e in xrange(2,len(l)+1)]:
        if not counts.has_key(name):
            counts[name] = 0
        if pop is None:
            pop = 1
        counts[name] += pop
    count += 1
    if count % 5000 == 0:
        sys.stderr.write('.')
sys.stderr.write('\n')
countries = countries.keys()
#print 'countries =', countries
#print 'counts =', counts

# Build the graph
sys.stderr.write('Build graph: 1. Add locations')
count = 0   # progress counter
G = iso_Graph(directed=True)

for location in locations:

    if location == '|0':
        continue

    location, pop = location.split(':')
    pop = string_to_int(pop)
    last_loc = ''
    #G.add_node(last_loc)
    for locname in location.split('|')[1:]:
        if verbose:
            print '**', locname
        loc = last_loc + '|' + locname
        if last_loc != '':
            #G.add_vertex(loc)
            frac = float(counts[loc]) /  float(counts[last_loc])
            try:
                w = -math.log(frac)
            except ValueError:
                sys.stderr.write("Bad value %f\n" % frac)
                sys.exit(1)
            if w<0.0: w=0.0
            G.add_edge(loc, last_loc, raise_exception=False, weight=w)
            if verbose:
                print 'new edge:', last_loc, '->', loc, 'w =', w
        G.add_edge(locname, loc, raise_exception=False)
        last_loc = loc

    count += 1
    if count % 5000 == 0:
        sys.stderr.write('.')
sys.stderr.write('\n')

sys.stderr.write('\nBuild graph: 2. Add names')
count = 0   # progress counter
for loc, names in names.iteritems():
    G.add_vertex(loc)
    for name in names:
        #G.add_vertex(name)
        G.add_edge(name, loc, raise_exception=False)
        count += 1
        if count % 5000 == 0:
            sys.stderr.write('.')

if verbose:
    for v in G.get().vs():
        print v['name'], '->', [s['name'] for s in v.successors()]
   
# Write dot file
sys.stderr.write('\nWrite out.dot\n')
G.make_dot('out.dot')

# save file
sys.stderr.write("Write pkl file '%s'\n" % ofn)
with open(ofn, 'w') as f:
    pickle.dump((G,countries), f)
