# -*- coding: utf-8 -*-
__author__ = 'duydo'
from pymongo import MongoClient


class MongoRepo(object):
    def __init__(self, host=None, port=None, **kwargs):
        if host and port:
            self._client = MongoClient(host, port, **kwargs)
        elif host:
            self._client = MongoClient(host, **kwargs)
        else:
            self._client = MongoClient(**kwargs)

        self._db = None
        self._collection = None

    @property
    def client(self):
        return self._client

    def use(self, db=None, collection=None):
        if db:
            self._db = self._client[db]
        if collection:
            self._collection = self._db[collection]

        return self

    '''def save(self, doc):
        return self._collection.save(doc)

    def insert(self, doc):
        return self._collection.insert(doc)

    def update(self, doc_id, doc):
        return self._collection.update({'_id': doc_id}, {'$set': doc}, upsert=False, multi=False)

    def remove(self, doc_id=None):
        if doc_id:
            self._collection.remove(doc_id)
        else:
            self._collection.remove()

    def count(self):
        return self._collection.count()

    def find_by_id(self, doc_id):

        if isinstance(doc_id, ObjectId):
            _id = doc_id
        else:
            _id = ObjectId(doc_id)

        for each in self.find({'_id': _id}).limit(1):
            return each
        return None

    def find(self, criteria=None, projections=None):
        if criteria and projections:
            return self._collection.find(criteria, projections)
        elif criteria:
            return self._collection.find(criteria)
        return self._collection.find()

    def find_one(self, criteria=None, projections=None):
        cur = self.find(criteria, projections).limit(1)
        cur = list(cur)
        if len(cur) > 0:
            return cur[0]
        return None'''

