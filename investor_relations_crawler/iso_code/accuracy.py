#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

__author__ = 'markus'

import sys

def make_none(s):
    if s=='' or s=='None':
        return None
    else:
        return s

count = 0
classified = 0
correct = 0
wrong = []
for raw_line in sys.stdin:
    l = map(make_none, raw_line.strip().split('\t'))
    if len(l) != 3:
        sys.stderr.write("Strange line: %s\n" % l)
        continue
    if l[1] is not None:
        count += 1
        if l[2] is not None:
            classified += 1
            if l[2] == l[1]:
                correct += 1
            else:
                wrong.append(raw_line.strip())
print "count      = %d" % count
print "classified = %d (%f %%)" % (classified, float(classified)/float(count)*100.)
print "correct    = %d (%f %%)" % (correct, float(correct)/float(classified)*100.)
sys.stderr.write("Wrong:\n")
for s in wrong:
    sys.stderr.write("%s\n" % s)