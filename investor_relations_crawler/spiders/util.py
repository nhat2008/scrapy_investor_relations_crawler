__author__ = 'nhat'
from urlparse import urlparse
from iso_EvalLocation import EvalLocation


class Util():

    load_pickle = None

    def __init__(self):
        self.load_pickle = EvalLocation('iso_location_pgm.pkl', "log_file")

    def get_domain(self, url):
        if not url:
            return ''
        parsed_uri = urlparse(url)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return domain

    def check_address(self, text):
        if text:
            return None
        result = self.load_pickle.eval(text)
        return result



