#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'markus'

# Dump the content of an excel sheet into tab delimited flat file (no fancy escaping and quoting, etc.)
# All it really does is deal with mangled unicode.

import xlrd
import sys
import re

r = re.compile('\[.*\]')

def fix(s):
    s = r.sub('',s)
    return s

if len(sys.argv)<2:
    sys.stderr.write('need at least one command line argument!\n')
    sys.exit(1)
    
for filename in sys.argv[1:]:
    sys.stderr.write("reading %s\n" % filename)
    try:
        book = xlrd.open_workbook(filename)
    except:
        sys.stderr.write("Failed to open %s\n" % filename)
        continue
    sheet=book.sheet_by_index(0)
    for i in xrange(sheet.nrows):
        try:
            row = [u'%s' % sheet.cell(i, j).value for j in xrange(sheet.ncols)]
        except:
            raise Exception("Error in '%s', could not read row\n" % filename)

        s = '\t'.join(['%s'%c for c in row])
        s = s.replace('\n','')
        print s.encode('utf-8')
        #except:
        #    sys.stderr.write(u"bad row #%s: %s\n" % (i, row))
        #    sys.exit(1)
