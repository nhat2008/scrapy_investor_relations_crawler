#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

__author__ = 'markus'

help = """
Evalaluate location information using the location engine specified in the graph stored in a pkl file.
The input is processed via stdin, a sequence of lines is processed,
each line has tab delimited columns. The first column has the location string, the other columns may
have other information that is repeated back in the result printout.
"""

import sys
import time
import argparse

import EvalLocation








# parse args
parser = argparse.ArgumentParser(description=help)
parser.add_argument("graph", help="The graph stored in a .pkl file")
parser.add_argument("-l", "--log", help="Write log file")
args = parser.parse_args()

graph_file = args.graph
log_file = args.log

sys.stderr.write("Loading graph file '%s'\n" % graph_file)
start = time.time()
eval = EvalLocation(graph_file, log_file)
load_graph_time = time.time()-start

total = 0           # count all rows
classified = 0      # count classified rows
start = time.time()
try:
    with open("2014.11.25.csv", "r") as myfile:
#    with open("test.txt", "r") as myfile:
        for raw_line in myfile.readlines():
#    for raw_line in sys.stdin:
            raw_line = raw_line.strip("\n")
            if raw_line == '' or raw_line.isspace():
                print "%s\t%s" % (raw_line, None)
                continue
            try:
                line = raw_line.strip().split("\t")
            except:
                sys.stderr.write("Cannot parse line '%s'. Skipping.\n" % line)
                continue
            str = line[0]
            e = eval.eval(str)
            print "%s\t%s" % (raw_line, e)
            total += 1
            if e is not None:
                classified += 1
except KeyboardInterrupt:
    sys.stdout.flush()

elapsed = time.time() - start
sys.stderr.write('total = %d\nclassified = %d\n' % (total, classified))
sys.stderr.write('%d locations/s\n' % round(float(total)/elapsed, 2))
sys.stderr.write('Loading time: %f\n' % round(load_graph_time,2))
