#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This is a wrapper around the ldig language detection for twitter messages ('tweets').
# See the README file and the reference therein for more details.

__author__ = 'hoan'
__version__ = "1.0"

from flask import request
from flask import Flask, jsonify

import EvalLocation








############################################# Wrapper around detector ########################################

def detect(l):
    model_path = 'location_pgm.pkl'
    result = []
    detector = EvalLocation(model_path)
    
    for line in l:
        if not line.strip():
            result.append(None)    
        else:
            result.append(detector.eval(line.strip()))

    return result


############################################# The Flask business ########################################

app = Flask(__name__)

@app.route('/')
def info():
    with open('README', 'r') as f:
        text = f.read()
        return text+"<h4>Version %s</h4>" % __version__


@app.route('/version')
def version():
    return '{"version":"%s"}' % __version__

@app.route('/detect', methods=['POST'])
def create_task():
    if not request.json:
        abort(400)
    # detect location
    detection = detect(request.json)
    return jsonify({'location': detection}), 201


if __name__ == '__main__':
    app.run(debug=True)
